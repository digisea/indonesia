# Indonesia

Materi keamanan digital dalam bahasa Indonesia

## Anonimitas dan Circumvention

1. [TOR Browser bahasa Indonesia](<https://tb-manual.torproject.org/id/>) 

## Data Leaks

1. [Pedoman keamanan dari Indonesialeaks](<https://www.indonesialeaks.id/pedoman-keamanan.html>) 

## Dokumentasi dan Panduan

1. [Security in the box](<https://securityinabox.org/id/>) {+quite old+}
1. [Internet Sehat serial siber](<https://internetsehat.id/cybersecurity/>)  
1. [Panduan Siber Center for Digital Society](<https://literasidigital.id/books/siberpedia-panduan-pintar-keamanan-siber-2/>)  
1. [Panduan Keamanan Kampanye Digital NDI](<https://www.ndi.org/sites/default/files/Cybersecurity%20for%20Campaigns%20Playbook%20-%20Indonesian.pdf>)   
1. [Panduan Informasi dan Tips Komponen untuk Keamanan dan Privasi](<https://www.combine.or.id/2018/03/19/panduan-informasi-dan-tips-komponen-untuk-keamanan-dan-privasi/>) from Combine {+translated from Tactical Tech+}  
1. [Panduan Informasi dan Tips Keamanan dan Privasi Dunia Digital](<https://www.combine.or.id/2018/03/19/panduan-informasi-dan-tips-keamanan-dan-privasi-dunia-digital/>) from Combine  
1. [Edisi 70 : Perlindungan Data Pribadi](<https://www.combine.or.id/2018/05/21/edisi-70-perlindungan-data-pribadi/>) from Combine  
1. [Budi Rahardjo, Keamanan Informasi](<http://budi.rahardjo.id/files/keamanan.pdf>)  
1. [Pentingnya kemitraan untuk keamanan digital](<http://onnocenter.or.id/pustaka/White%20Paper%20TikTok%20-%2019%20Okt2020%20-%20%20Pentingnya%20Kemitraan%20Untuk%20Memperkuat%20Keamanan%20Siber%20Indonesia.pdf>) {+CFDS, Onno Center+}
1. [Panduan Memilih Aplikasi Percakapan yang Melindungi Privasi](<https://id.safenet.or.id/2021/01/panduan-memilih-aplikasi-percakapan-yang-melindungi-privasi/>) from Safenet
1. [Tips internet aman dari Google](<https://safety.google/intl/id/security/security-tips/>)
1. [Tips internet aman dari Unicef untuk anak](<https://www.unicef.org/indonesia/id/child-protection/tips-aman-berinternet>)



## Forensik

1. [Penelitian Finfisher di Indonesia](<https://citizenlab.ca/docs/finfisher-indonesia.pdf>)  

# Gender dan Internet

1. [Buku Saku KBGO](<https://web.tresorit.com/l/6BdDo#OD-9z2OdX0KP32wZIW2M9g>) 
1. [Buku Saku Pinjol & KBGO](<https://bantuanhukum.or.id/wp-content/uploads/2021/02/2020_Buku-saku-Pinjol.pdf>)

## Keamanan organisasi, profesi

1.[Modul perlindungan digital](<https://advokasi.aji.or.id/safety>)  
2.Keamanan jurnalis {+engagemedia+} - [1](<https://engagemedia.org/2017/persepsi-tentang-keamanan-dan-keselamatan-profesi-wartawan-di-filipina/>) | [2](<https://engagemedia.org/2017/apakah-profesi-wartawan-aman-di-indonesia/>)    
3.[Keamanan Digital untuk Jurnalis (Webinar) dari pulitzercenter.org](https://pulitzercenter.org/event/keamanan-digital-untuk-jurnalis-webinar)  
Jurnalis  
4.[Panduan keamanan jurnalis dari Facebook](https://id-id.facebook.com/business/help/252441975867823)

## Komunikasi aman

1. [Panduan Signal Bhs Indo - Localizationlab](<https://static1.squarespace.com/static/542b02d7e4b0a92b527f5750/t/5c0806ae03ce649b420345ae/1544029870911/Bahasa+Indonesia+-+Signal+for+Android.pdf>)  

# Panduan Keamanan internet pendek dari media/agency

1. Tips aman dari [Kumparan](<https://kumparan.com/skyegrid-id/10-tips-jaga-keamanan-dan-privasi-di-internet-1rhu3YoLuW3>)
1. 10 tips aman dari [DW](<https://www.dw.com/id/10-tips-keamanan-berinternet/g-37370906>)
1. Tips aman dari Kompas [1](<https://tekno.kompas.com/read/2020/04/24/11040077/7-tips-aman-berinternet-saat-harus-bekerja-dari-rumah?page=all>) | [2](<https://tekno.kompas.com/read/2019/12/11/09430057/4-cara-menjaga-keamanan-data-pribadi-dari-kejahatan-siber?page=all>)
1. 9 Tips aman [Suara](<https://www.suara.com/tekno/2019/08/28/073500/9-tips-jaga-keamanan-dan-privasi-berinternet-ala-google?page=all>)
1. Tips dari [Fimela](<https://www.fimela.com/lifestyle-relationship/read/4042630/10-tips-menjaga-keamanan-dan-privasi-ketika-menggunakan-internet>)
1. Tips dari [Hitekno.com](<https://www.hitekno.com/internet/2019/04/17/072613/harus-tahu-ini-10-tips-keamanan-internet-dan-privacy>)





  

 





 
